<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
		try {
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			// Creating PDO connection
			$this->db = new PDO('mysql:host=localhost;dbname=assignment1;', 'root', ''); 
		}
		} catch (PDOException $ex) {
			echo $ex->getMessage();	
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try {
		$booklist = array();    // Create array for booklist
		
		$stmt = $this->db->query("SELECT * FROM book");		// Selects all books from the table.
		$booklist = $stmt->fetchAll(PDO::FETCH_OBJ);		// Returns booklist.
		
		return $booklist;
		} catch (PDOException $ex) {
			echo $ex->getMessage();	
		}
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {	
		try { 
			$book = null;
			if (isset($id) AND is_numeric($id)) {
				$stmt = $this->db->prepare('SELECT * FROM book WHERE id = ?');
				$stmt->execute(array($id));
				if($var = $stmt->fetch(PDO::FETCH_ASSOC)) {				// If able to fetch as associative.
					$book = new Book($var['title'], $var['author'], 				
							 $var['description'], $var['id']);
				}
				return $book;											// Returns book if id is found.
			} 
			else {
				$view = new errorView();
				$view->create();
				return null;											// Returns null if id isn't found.
			} 			
		} catch (PDOException $ex) {
			echo $ex->getMessage();	
		}    
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try {
			if ($book->title != '' AND $book->author != '' AND isset($book->id) AND is_numeric($book->id)) { // If valid input.
			if ($book->description = '') {$book->description = null;}										// Sets desc to null if blank.
		$stmt = $this->db->prepare("INSERT INTO book(id,title,author,description) VALUES(:id,:title,:author,:description)"); // Prepares query.
		$stmt->execute(array(':id' => NULL, ':title' => $book->title, ':author' => $book->author, ':description' => $book->description));	// "Binds" values to the book.
		$book->id = $this->db->lastInsertId();  	// Sets the next available id to the new book.
			} 
			else {
				$view = new errorView();		// Error if invalid input.
				$view->create();
			}
		} 
		catch (PDOException $ex) {
			echo $ex->getMessage();	
		}
	}
    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		try {
			if ($book->title != '' AND $book->author != '' AND isset($book->id) AND is_numeric($book->id)) { // If valid input
			if ($book->description = '') {$book->description = null;}										// Sets des to null if blank.
		$stmt = $this->db->prepare("UPDATE book SET title=?, author=?, description=? WHERE id=?");	// Prepares data to be updated.
		$stmt->execute(array($book->title, $book->author, $book->description, $book->id));		// Adds new data about the selected book.
			} 
			else {
				$view = new errorView();		// Error if invalid input.
				$view->create();
			}
		}
		catch (PDOException $ex) {
			echo $ex->getMessage();	
		}
     }
	

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try {
		$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");	// Prepares book to be deleted.
		$stmt->bindValue(':id', $id, PDO::PARAM_STR);	// Binds the id.
		$stmt->execute();	// Deletes the book.
		}
		catch (PDOException $ex) {
			echo $ex->getMessage();	
		}
    }
	
}

?>